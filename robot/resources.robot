*** Settings ***
Documentation     An example resource file
Library           Selenium2Library

*** Variables ***
${HOST}           http://localhost
${BROWSER}        Firefox
${MAINPAGE}       Portfolio Webpage

*** Keywords ***
Open Web Page
    [Arguments]         ${URL}      ${TITLE_SHOULD}
    [Documentation]     Opens browser to main page

    Open Browser        ${URL}      ${BROWSER}
    Title Should Be     ${TITLE_SHOULD}

Start Docker


Kill Docker


Change Language
    [Documentation]             Change and confirm changing language
    [Arguments]     ${label}    ${shouldBe}
    Select From List            xpath=//select[@id="language_select"]   ${label}
    Sleep                       2 seconds
    Element Text Should Be      xpath=//h1[@class="SiteHeader"]         ${shouldBe}

Open Mainpage
    [Documentation]         Setup selenium speed and open browser
    Set Selenium Speed      1
    Open Web Page           ${URI}  Student Portfolio

Find Component
    [Documentation]             Find a html component with id $id
    [Arguments]                 ${HTML_ID}
    Element Should Be Visible   identifier=${HTML_ID}