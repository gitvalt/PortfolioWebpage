*** Settings ***
Documentation     An example resource file
Library           Selenium2Library
Resource          resources.robot

*** Variables ***
${URI}              http://localhost


*** Test Cases ***
Open Project
    [Documentation]             Open a project from project view
    [Setup]                     Open Mainpage
    [Teardown]                  Close Browser
    Open Project view
    Select From List By Index   xpath=//button[@data-type="navbutton"]   0               

Test Gallery

*** Keywords ***
Open Project view
    [Documentation]                 Open project viewer
    Click Element                   id=contentviewer_button
    Element Should Be Visible       identifier=ContentViewer
    Element Should Be Visible       identifier=NavList_Box
