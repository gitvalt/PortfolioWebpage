*** Settings ***
Documentation     An example resource file
Library           Selenium2Library
Resource          resources.robot

*** Variables ***
${URI}              http://localhost

${FI_TITLE}         OPISKELIJAN ETUSIVUT
${ENA_TITLE}        STUDENTS FRONTPAGE


*** Test Cases ***
Open Browser and Mainpage
    [Teardown]      Close Browser
    Open Mainpage


Find Components
    [Teardown]      Close Browser
    [Setup]         Open Mainpage
    Find Component  shownContent
    Find Component  tabulator_Box
    Find Component  profile_Box
    Find Component  Footer_Box
    Element Should Not Be Visible   identifier=loading_Box


Tabulator Changing
    [Teardown]                      Close Browser
    [Setup]                         Open Mainpage
    Element Should Be Visible       identifier=Description_box
    Tabulator Changed               school_button           Education_Box
    Tabulator Changed               description_button      Description_box
    Tabulator Changed               skills_button           skills_Box


Changing Language
    [Teardown]          Close Browser
    [Setup]             Open Mainpage
    Change Language     Englanti      ${ENA_TITLE}
    Change Language     English       ${ENA_TITLE}
    Change Language     Finnish       ${FI_TITLE}
    Change Language     Suomi         ${FI_TITLE}
    


*** Keywords ***
Open Mainpage
    [Documentation]         Setup selenium speed and open browser
    Set Selenium Speed      1
    Open Web Page           ${URI}  Student Portfolio

Find Component
    [Documentation]             Find a html component with id $id
    [Arguments]                 ${HTML_ID}
    Element Should Be Visible   identifier=${HTML_ID}

Tabulator Changed
    [Documentation]                 Change and confirm tabulator tab change
    [Arguments]                     ${BUTTON}   ${DIV}
    Click Element                   id=${BUTTON}
    Element Should Be Visible       identifier=${DIV}

Change Language
    [Documentation]             Change and confirm changing language
    [Arguments]     ${label}    ${shouldBe}
    Select From List            xpath=//select[@id="language_select"]   ${label}
    Sleep                       2 seconds
    Element Text Should Be      xpath=//h1[@class="SiteHeader"]         ${shouldBe}
    