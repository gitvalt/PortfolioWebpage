# Javascript gallery

## Developer
Valtteri Seuranen aka. gitvalt

## Introduction
Goal of this project was to create a portfolio website, from where people could see basic information about me and projects that i have done. The site would work like a CV. Project was created with React so that the site would be easily updateable and configuratable. Also the site would provide content in Finnish and English. 

The website will be available to public through Gitlab's static pages at address: https://gitvalt.gitlab.io/PortfolioWebpage

## Table of content

    dist/   Finished product shown in a html page. 
        assets/
            documentation/  Contains documentation about the websites design
            gallery/        
            logo/           Logos used in website
            projects/       Media used in project view component.
            config.json     Websites general setting
            ena.json        Website custom content translations in english
            fi.json         Website custom content translations in finnish
        index.html      Mainpage
        bundle.js  The app packed with Webpack

    src/    App files
        config/         Hard coded default settings and language translations
        css/            Website styling
        js/             Javascript components

    .gitlab-ci.yml      For deploying this repository as gitlab static page.
    build               Build web.config.js
    docker-compose.yml
    package.json
    web.config.js
    export              For exporting contents of dist to zipfile

## Running
Install docker and docker-composer and then run docker-compose.yml.
Docker should start nginx-server at localhost:80.

## Export
Run "export" script to create movable zipfile
Move created zipfile to desired locations
Run docker-compose.yml

## Development
1. Install depencies with npm install
2. if not installed, install npm npx
3. make desired changes
4. Run build script