export class General {
    constructor() {
        this.Debug = false;
        this.Language = "fi" //use short version (fi, ena...)
        this.ProfileImage = "assets/profile.jpg"
        this.InLabranet = false;

        //Tabulator available views
        this.ContentViews = {
            Description: 0,
            Skills: 1,
            Experience: 2,
            Education: 3,
        };
        //Tabulator default view
        this.ContentBoxDefault = this.ContentViews.Description;
        this.DefaultView = "mainpage"
    }
}