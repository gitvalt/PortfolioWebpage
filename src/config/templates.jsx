export class CourseSettings {
    constructor(linkArray) {
        if (!(linkArray instanceof Array)) return new Error("CourseSettings.constructor: Course links not in a array");
        this.CourseLinks = linkArray;
    }
    get Count() {
        return this.CourseLinks.length;
    }
}

export class SkillList {
    constructor(skillArray) {
        if (!(skillArray instanceof Array)) return new Error("Skills.constructor: Not a Array");
        this.Skills = skillArray;
    }
    //array.includes not supported by IE 11.
    tagExists(tag, array){
        array.forEach((item) => {
            if(item == tag) return true;
        });
        return false;
    }
    //return list of different tags used by skills
    GetTags(){
        var tags = [];
        if(this.Skills.length == 0) return tags;
        
        this.Skills.forEach((item) => {
            //only add tag once (no duplicates)
            if(!this.tagExists(item, tags)) tags.push(item.Tag);
        });
        return tags;
    }
    GetItemsWithTag(tag){
        //console.debug(`Tag received: ${tag}`);
        var tagged = [];
        this.Skills.forEach((item) => {
                if(item.Tag == tag) tagged.push(item);
        });
        //console.debug(`Tagged elements: ${tagged.length}`);
        var wrapper = {Title: tag, Values: tagged};
        return wrapper;
    }
}

export class Project {
    constructor(linkTitle, Title, hostsite, link, description, images) {
        this.Link_Title = linkTitle;
        this.Header_Title = Title;
        this.Link = link;
        this.HostSite = hostsite;
        this.Description = description;
        this.Images = images;
    }
    get Title(){
        if(this.Header_Title != null) return this.Header_Title;
        else if(this.Link_Title != null) return this.Link_Title;
        else return "Undefined Title";
    }
    get LinkTitle() {
        if(this.Link_Title == null && this.Header_Title == null) return undefined;
        else if(this.Link_Title == null && this.Header_Title != null) return this.Header_Title;
        else return this.Link_Title;
    }
}

export class Projects {
    constructor(projectArray){
        if(!(projectArray instanceof Array)) return new Error("class.Projects: Constructor requires a Array of projects");
        this.Projects = projectArray;
    }
    doesExists(projectTitle){
        if(this.Projects.length == 0) return false;
        var result = false;
        this.Projects.forEach(project=>{
            if(project.Header_Title == projectTitle || project.Link_Title == projectTitle) {
                result = true;
            }
        });
        return result;
    }
    findProject(project) {
        if(!(this.Projects instanceof Array) || this.Projects.length == 0) {
            return new Error("Not an array or null");
        }
        var result;
        this.Projects.forEach(item=>{
            if(item.Title == project || item.Link_Title == project) {
                result = item;
            }
        });
        return result;
    }
}