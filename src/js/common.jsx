import * as React from 'react';
import "../css/common.scss";

//ErrorComponent: Renders a simple error message.
/*
    Properties: title, message
*/
export class ErrorComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div id="Error_Box">
                <h1>
                    {this.props.title}
                </h1>
                <p>
                    {this.props.msg}
                </p>
            </div>
        );
    }
}

//ExternalLinks: Renders a list of links to other websites. 
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining should logs be printed
    links = array of links that should be shown
        link {
            Title,  
            Link,   Url to webpage
            Logo,   Path to logo imagefile
        }
*/
export class ExternalLinks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }

    //Return jsx components from given array of links
    parseLinks(links) {
        var errs = this.props.lang.Errors;
        if (this.state.debug) {
            console.log(`ExternalLinks.parseLinks: Received links: `);
            console.info(links);
        }
        var err = <ErrorComponent title={errs.LoadingFailed.Title} msg={errs.LoadingFailed.Content} />;
        if (links == null) {
            console.error("ExternalLinks.parseLinks: props.links is null");
            return err;
        }
        if (links.lenght == 0) {
            console.error("ExternalLinks.parseLinks: No links defined");
            return err;
        }
        return links.map(item =>
            <li key={item.Title}>
                <a href={item.Link}><img src={item.Logo} alt={item.Title + "_logo"} className="eLink_Image"></img></a>
            </li>
        );
    }

    render() {
        this.state.debug = this.props.debug;
        var parsedLinks = this.parseLinks(this.props.links);
        if (this.state.debug) console.log("ExternalLinks.render: Rendering ExternalLinks component"), console.log(parsedLinks);
        return (
            <div id="externalLinks_Box">
                <ul>
                    {parsedLinks}
                </ul>
                <div className="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="en_US" data-type="horizontal" data-theme="dark" data-vanity="valtteri-seuranen-mainpage"><a className="LI-simple-link" href='https://fi.linkedin.com/in/valtteri-seuranen-mainpage?trk=profile-badge'>Valtteri Seuranen</a></div>
                
            </div>
        );
    }
    componentDidMount() {
         //require("./linkedin_profile.js")
        var old;
         if(old = document.getElementById("LinkedInJS"), old != null)
        {
            old.remove();
        }

         var addLinkedInScript = document.createElement("script");
         addLinkedInScript.setAttribute("id", "LinkedInJS");
         addLinkedInScript.setAttribute("src", "https://platform.linkedin.com/badges/js/profile.js");
         document.body.appendChild(addLinkedInScript);
    }
}

//Header: Renders webpage header
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining should logs be printed
    changeView = callback for changing currently shown view (profile, list of done work)
    changeLang = callback for changing current language to something else.
*/
export class Header extends React.Component {
    constructor(props) {
        super(props);
        this.languageClick.bind(this);
        this.changeViews.bind(this);
        //sets contentview as default
        this.state = {
            debug: false,
            currentview: "contentview",
        };
    }
    //when language is set to change --> callback to mainpage-component
    languageClick(e) {
        if (this.state.debug) console.info(`Clicked 'change language button' button`);
        this.props.changeLang(e.target.value);

    }

    //when view is set to change --> callback to mainpage-component
    changeViews(view, event) {
        if (this.state.debug) console.info(`Clicked '${view}' button`);
        this.props.changeView(view);
    }
    componentWillMount() {
        this.state.debug = this.props.debug;
    }
    render() {
        if (this.state.debug) console.log("Header.render: Rendering Header component");
        return (
            <div id="header_Box">
                <h1 className="SiteHeader">
                    {this.props.lang.Header}
                </h1>
                <div id="languagebuttons_Box">
                    <ul id="viewbuttons">
                        <li><button id="mainpage_button" type="button" onClick={(e) => this.changeViews("mainpage", e)}>{this.props.lang.Mainpage}</button></li>
                        <li><button id="contentviewer_button" type="button" onClick={(e) => this.changeViews("contentviewer", e)}>{this.props.lang.ProjectView}</button></li>
                    </ul>
                    <select onChange={(e) => this.languageClick(e)} id="language_select">
                        <option value="">{this.props.lang.selectLanguage}</option>
                        <option value="fi">{this.props.lang.Finnish}</option>
                        <option value="ena">{this.props.lang.English}</option>
                    </select>
                </div>
            </div>
        );
    }
}

//Footer: Renders webpage footer
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining should logs be printed
    inLabranet = boolean defining should student ID be shown
*/
export class Footer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        var lang = this.props.lang
        if (this.props.debug) console.log("Footer.render: Rendering Footer component");
        if (this.props.InLabranet) return (<div id="Footer_Box">{lang.Student.Name}, {lang.Student.AccountID}, {lang.Student.Group}</div>);
        else return (<div id="Footer_Box">{lang.Student.Name}</div>);
    }
}