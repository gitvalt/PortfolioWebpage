import * as React from 'react';


import "../css/common.scss";
import "../css/gallery.scss";

import {Error} from "./common.jsx";

//Gallery is not implemented
export class Gallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }
    //parser as separate function?
    parseImages() {
        var errmsg = <Error title="Images not defined" msg="Could not read or parse images from configuration" />
        var imgs = this.props.images.ImageList;
        if (this.state.debug) console.info(imgs);
        if (imgs == null || imgs.lenght == 0) {
            console.error("Gallery.parseImages: props.images is empty");
            return errmsg;
        }
        if (!(imgs instanceof Array)) {
            console.error("Gallery.parseImages: props.images is not an Array");
            return errmsg;
        }
        return imgs.map(image=>
            <img key={image.Title} src={image.ImagePath}></img>
            );
    }
    render() {
        this.state.debug = this.props.debug;
        var images = this.parseImages(); 
        return (
        <div id="gallery_Box">
                <div id="galleryList_Box">
                    {images}
                </div>    
        </div>
        );
    }
    componentDidMount() {
        
    }

}