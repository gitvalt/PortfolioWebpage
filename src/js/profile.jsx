import * as React from 'react';

import "../css/common.scss";
import "../css/profile.scss";

import {ExternalLinks} from "./common.jsx";

//Profile: Component shows basic information about the user
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining if logs will be printed
    img = profile image
*/
export class Profile extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        if (this.props.lang == null) throw new Error("Lang is null")
    }
    render() {
        var lang = this.props.lang;
        if (this.props.debug == "true") {
            console.log(`Profile.render: Received props:`)
            console.log(this.props)
        }
        return (
            <div id="profile_Box">
                <img src={this.props.image} className="profile_img" alt={lang.Profile.ImageAlt} />
                <div id="profile_Content">
                    <table>
                        <tbody>
                            <tr>
                                <td colSpan="2">
                                    {lang.StudentLabel}, {lang.Student.Name}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {lang.AccountIDLabel}:
                            </td>
                                <td>
                                    {lang.Student.AccountID}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {lang.GroupLabel}:
                            </td>
                                <td>
                                    {lang.Student.Group}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {lang.EmailLabel}:
                            </td>
                                <td>
                                    {lang.Student.StudentEmail}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2">
                                    <p>{lang.AltEmailLabel}: {lang.Student.PersonalEmail}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <ExternalLinks debug={this.props.debug} lang={this.props.lang} links={this.props.lang.ExternalLinks} />
                </div>
            </div>
        );
    }
}
