//Libraries
import * as React from 'react';
import $ from 'jquery';

//Components
import { General, Student } from "../config/settings.jsx";
import { ErrorComponent, Header, Footer } from "./common.jsx";
import { Profile } from "./profile.jsx";
import { CourseLink } from "./course.jsx";
import { Tabulator, Skills } from "./tabulator.jsx";
import { WorkView } from "./workviewer.jsx";

//Language files and default settings
import finnish from "../config/finnish.json";
import english from "../config/english.json";
import general from "../config/general.json";

//Stylesheets
import "../css/common.scss";
import "../css/content.scss";
import "../css/courses.scss";
import "../css/gallery.scss";
import "../css/profile.scss";
import "../css/tabulator.scss";


//Mainpage: Component to contain all webpage components.
export class Mainpage extends React.Component {
    constructor(props) {
        super(props);
        this.changeLanguage = this.changeLanguage.bind(this)
        this.changeContentView = this.changeContentView.bind(this)
        this.state = {
            debug: false,               //should logs be printed
            initialConfig: true,        //is this first time that configurations are loaded

            getSettings: true,          //should load settings?
            getSettings_ongoing: false, //waiting for jquery to load settings? (if loading, do not repeat loading while previous is still running)
            settings: null,             //contains loaded settings

            fetchLanguage: true,            //should language file be loaded?
            fetchLanguage_ongoing: false,   //is loading ongoing?
            language: null,                 //language file

            view_enum: null,                //currently shown webview as string
            view: null,                     //currently shown webview (profile, portfolios...)
        };
    }

    //set default settings
    componentWillMount() {
        console.log("Mainpage.componentWillMount: Rendering UI...")
        var config = this.getConfig();
        this.state.initialConfig = false;
        this.state.settings = config;
        if (config.Debug) console.log("Mainpage.componentWillMount: Setting debug mode true"), this.state.debug = true;
    }

    //get general settings and set state.settings with results
    //success -> returns false and sets state (no update), failure -> return Error
    getConfig() {
        //Async get custom values from dist/assets
        this.state.getSettings_ongoing = true;
        $.getJSON("./assets/config.json", function (data) {
            if (this.state.debug) console.log("Mainpage.pre_config: Reading config from json");
        }.bind(this)).done(function (data) {
            if (this.state.debug) console.log("Mainpage.pre_config: Json config found"), console.log(data);
            this.setState({ settings: data, getSettings: false, getSettings_ongoing: false, debug: data.Debug });
        }.bind(this)).fail(function (data) {
            console.error("Mainpage.pre_config: Failed to load config from json");
        }.bind(this));

        //while real config is read async, use local hardcoded settings
        var general;
        try {
            general = new General();
        } catch (err) {
            console.error(`Mainpage.render: Failed to read config. Error message: '${err.message}'`);
            general = <ErrorComponent title="Failed to read config" msg="Failed to read config" />
        }
        if (this.state.debug) console.log("Mainpage.getConfig: Returning element:"), console.log(general);
        return general;
    }

    //fetches language settings, onsuccess language state is set
    //success -> sets language state and returns null
    //failure -> returns error 
    setLanguage(language) {
        if (this.state.debug) console.log(`Mainpage.setLanguage: Trying to set language state as '${language}'`);
        var getL = this.getLanguage(language); //getLanguage returns [0] languagefile and [1] path to language specific settings in dist/assets folder (example: contains Courses, projects, descriptions...)

        if (getL[0] instanceof Error) {
            if (this.state.debug) console.error("Mainpage.setLanguage: Setting language returned an error");
            return getL[0];
        } else {
            if (this.state.debug) console.log("Mainpage.setLanguage: Successfully fetched language");
            this.state.language = getL[0];

            if (this.state.debug) console.log("Mainpage.setLanguage: Fetching json file");
            this.configureLanguage(getL[1]);
            return null;
        }
    }

    //Get correct language settings. If fails returns error
    /*
        returns language phrases and path to language specific information
    */
    getLanguage(language) {
        var newLang, filepath;
        if (this.state.debug) console.log(`Mainpage.getLanguage: fetching language: '${language}'`);
        if (language == null) return new Error("Mainpage.getLanguage: Cannot set language as empty");
        switch (language) {
            case "fi":
                newLang = finnish;
                filepath = "fi.json";
                break;
            case "ena":
                newLang = english;
                filepath = "ena.json";
                break;
            case undefined:
                return new Error(`Mainpage.getLanguage: Undefined language! '${language}'`);
                break;
            default:
                return new Error(`Mainpage.getLanguage: Unknown language!`);
                break;
        }

        newLang.Student = general.Student;
        return [newLang, filepath];
    }

    //get language settings from dist/assets
    configureLanguage(filename) {
        this.state.fetchLanguage_ongoing = true;
        var configured = $.getJSON("./assets/" + filename, function (data) {
            if (this.state.debug) console.log("Mainpage.configureLanguage: Fetching language content from json");
        }.bind(this)).done(function (data) {
            if (this.state.debug) console.log("Mainpage.configureLanguage: Json language file found"), console.log(data);
            var lang = this.state.language;
            lang.Skills = data.Skills;
            lang.Courses = data.Courses;
            lang.ExternalLinks = data.ExternalLinks;
            lang.WorkExperiences = data.Experiences;
            lang.Educations = data.Educations;
            lang.Projects = data.Projects;
            this.setState({ language: lang, fetchLanguage: false, settings: this.state.settings, fetchLanguage_ongoing: false });
        }.bind(this)).fail(function (data) {
            console.error("Mainpage.configureLanguage: Failed to load config from json");
        }.bind(this));
        return configured;
    }

    //change language to $lang
    changeLanguage(lang) {
        console.clear();
        if (this.state.debug) console.log(`Mainpage.changeLanguage: Changing language from '${this.state.settings.Language}' to '${lang}'`);
        if (lang == null) {
            console.error(`Mainpage.changeLanguage: Language '${lang}' was not found.`);
            return;
        } else {
            //get current settings, change language in settings, add modified settings as current setting.
            var fetchS = this.state.settings;
            fetchS.Language = lang;
            this.setState({ settings: fetchS, view_enum: this.state.view_enum, fetchLanguage: true });
        }
    }

    //run before rendering component
    pre_config() {
        if (this.state.debug) console.log("Mainpage.pre_config: Executing preconfigurations. Printing states:"), console.log(this.state);

        //update settings only if error or empty --> json loaded config not overwriten.
        if (this.state.settings instanceof ErrorComponent || this.state.settings == null) {
            var config = this.getConfig();
            if (config instanceof ErrorComponent) {
                return config;
            } else {
                this.state.config = config;
            }
        }
        return null;
    }

    //run before rendering component
    pre_language() {

        //if language is already being loaded or data is set not to update
        if (this.state.fetchLanguage_ongoing || this.state.fetchLanguage == false) {
            if (this.state.debug) console.log("Mainpage.pre_language: Language is already updating. Skip");
            return null;
        }

        var lang = this.setLanguage(this.state.settings.Language)
        if (this.state.debug) console.info(lang);

        if (lang instanceof Error) {
            console.error(`Mainpage.render: Failed to read language. Error message: '${lang.message}'`)
            if (this.state.initialConfig) return <ErrorComponent title="Failed to read language" msg="Failed to read language" />;
        }

        //debug msg
        if (this.state.debug) {
            console.log("Mainpage.prelanguage: Successfully fetched language files:")
            console.info(this.state.language)
        }
        return null
    }

    //run before rendering component
    pre_view() {
        var view = this.state.view;
        if (view == null) this.state.view_enum = "default", this.state.view = this.getContentView("default");
        else if (this.state_view == null) this.state.view = this.getContentView(this.state.view_enum);
    }

    //run pre_* functions
    prerender() {
        if (this.state.debug) console.info(this.state);

        //pre_* functions return null if success, ErrorComponent if fail
        if (this.state.debug) console.info("Mainpage.prerender: Loading config");
        var config = this.pre_config()
        if (config != null) return config;

        if (this.state.debug) console.info("Mainpage.prerender: Loading language");
        var lang = this.pre_language();
        if (lang != null) return lang;

        this.pre_view();
    }

    changeContentView(view) {
        console.clear();
        if (this.state.debug) console.log("Mainpage.changeContentView: Changing content view");
        if (view == null) {
            console.error("Mainpage.changeContentView: Cannot change to null");
            return;
        }
        var content = this.getContentView(view);
        this.setState({ view: content, view_enum: view });
    }

    getContentView(view) {
        if (this.state.debug) console.log("Mainpage.getContentViewer: Selecting content view. ", view);
        if (view == null) {
            console.error("Header.changeview: View not specified");
            return;
        }
        const courses = <CourseLink lang={this.state.language} courses={this.state.language.Courses} debug={this.state.debug} />;
        var course = null;
        if (this.state.settings.InLabranet) course = courses;
        const mainpage =
            (<div>
                <Profile lang={this.state.language} image={this.state.settings.ProfileImage} debug={this.state.debug} />
                {course}
                <Tabulator lang={this.state.language} settings={this.state.settings} debug={this.state.debug} />
            </div>);
        switch (view) {
            case "mainpage":
                return mainpage;
                break;
            case "contentviewer":
                return (
                    <div>
                        <WorkView lang={this.state.language} debug={this.state.debug} />
                    </div>
                );
                break;
            default:
                if (this.state.debug) console.info("Header.changeview: Undefined view. Returning default view");
                return this.getContentView(this.state.settings.DefaultView);
                break;
        }
    }

    render() {
        var prepare = this.prerender()
        //if not null --> ErrorComponent returned --> Render only ErrorComponent
        if (prepare != null) return prepare;
        if (this.state.debug) console.log("Mainpage.render: Preparations done. Rendering..."), console.log(this.state);

        const mainview = (
            <div id="DisplayDiv">
                <div id="HeaderDiv" />
                <div id="Content">
                    <Header lang={this.state.language} changeLang={this.changeLanguage} changeView={this.changeContentView} debug={this.state.debug} />
                    <div id="ContentView">
                        {this.state.view}
                    </div>
                    <Footer lang={this.state.language} debug={this.state.debug} InLabranet={this.state.settings.InLabranet} />
                </div>
            </div>
        );

        const loadingView = (
            <div id="loading">
                <h1>Loading Configurations</h1>
            </div>
        )

        if (!this.state.fetchLanguage && !this.state.getSettings) {
            if (this.state.debug) console.log("Mainpage.render: Rendering mainview");
            return mainview;
        }
        else return loadingView;
    }
}