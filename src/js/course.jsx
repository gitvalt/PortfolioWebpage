import * as React from 'react';
import "../css/common.scss";
import "../css/courses.scss";


//CourseLink: renders a flexbox containing button linking to course folders
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining should logs be printed
    courses = array of courses
    course = {
        Course: Name of the course
        Link: Filepath to course
    }
*/
export class CourseLink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }

    componentWillMount() {
        this.state.debug = this.props.debug;
    }

    //handleCourses: parse array with object(Course:string, Link:string) to <a /> hyperlink list
    handleCourses() {
        var courses = this.props.courses
        if (this.state.debug) {
            console.log("CourseLink.handleCourse: Starting course link parsing. Data to parse:")
            console.log(courses)
        }
        return courses.map(course =>
            <li key={course.Course}>
                <a href={course.Link}>{course.Course}</a>
            </li>
        );
    }

    render() {
        if (this.props.debug) console.log("Courses.render: Rendering course components");
        var parsedCourses = this.handleCourses();
        return (
            <div id="courses_Box">
                <ul>
                    {parsedCourses}
                </ul>
            </div>
        );
    }
}
