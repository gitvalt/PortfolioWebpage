import * as React from 'react';

import "../css/common.scss";
import "../css/content.scss";
import {Projects} from "../config/templates.jsx";
import "../css/gallery.scss";

//WorkView: Component wraps up NavList and ContentViewer components
export class WorkView extends React.Component {
    constructor(props) {
        super(props);
        this.changeProject = this.changeProject.bind(this)
        this.state = {
            debug: false,
            projects: null,
            project_found: false,
            current_project: null,
        };
    }
    componentWillMount(){
        this.state.debug = this.props.debug;
        if(this.props.lang == null) {
            console.error("Language configuration not defined");
            throw new Error("Language configuration not defined");
        }
        this.state.projects = this.props.lang.Projects;
    }

    //Change currently shown project
    changeProject(project, event){
        console.clear();
        if(this.state.debug) console.log("WorkView.changeProject: Received 'change component' call. Changing into: " + project);
        
        if(project == null){
            console.error("WorkView.changeProject: Cannot change project. Project not defined");
            return;
        }

        console.log(this.state);
        var projects_v = new Projects(this.state.projects);
        var proj = projects_v.findProject(project);
        if(this.state.debug) console.log("WorkView.changeComponent: fetched project"), console.log(proj);
        
        if(proj == null) console.error("WorkView.changeProject: Could not find project."), this.state.project_found = false;        
        else {
            if(this.state.debug) console.log("WorkView.changeComponent: updating currently watched project"), console.log(proj);    
            this.setState({current_project: proj, project_found: true});
        }

    }

    //run before render
    init(){
        if(this.state.debug) console.log("WorkView.init: Start init"), console.log(this.state);

        //on render update projects from language settings
        this.state.projects = this.props.lang.Projects;
        if(this.state.projects == null) {
            if(this.state.debug) console.debug("WorkView.init: Projects null returning...");
            if(this.state.current_project != null) this.state.project_found = false;
            return;
        }

        //on init nothing selected --> continue
        if(this.state.current_project == null || this.state.current_project == undefined){
            if(this.state.debug) console.info("WorkView.init: Current project are empty"), this.state.project_found = false;
            return;
        }

        if(this.state.debug) console.info("WorkView.init: current_project:"), console.log(this.state.current_project);
        
        var projects_v = new Projects(this.state.projects);
        if(projects_v.doesExists(this.state.current_project.Header_Title) == false){
            this.state.project_found = false;
            console.error("WorkView.init: Could not find current_project from projects")
            return;
        }
        this.state.project_found = true;
        if(this.state.debug) console.log("WorkView.init: Init done. Project has been found from language settings"), console.log(this.state.projects);
    }

    render() {
        this.init();
        if(this.state.debug) console.log("WorkView.render: Rendering viewer component");
        return (
        <div id="WorkViewer_Box">
            <NavList projects={this.state.projects} changeproject={this.changeProject} c_project={this.state.c_project} lang={this.props.lang} debug={this.state.debug} />
            <ContentViewer c_project={this.state.current_project} found={this.state.project_found} lang={this.props.lang} debug={this.state.debug}/>
        </div>
        );
    }
}

//NavList: Component shows list of available project views
export class NavList extends React.Component {
    constructor(props) {
        super(props);
        this.linkbutton = this.linkbutton.bind(this);
        this.state = {
            debug: false,
        };
    }
    
    componentWillMount() {
        this.state.debug = this.props.debug;
        if(this.state.debug) console.info(this.props);
    }

    //linkbutton: creates and returns a linkbutton from given props
    linkbutton(props) {
        var is_selected, linktitle;
        is_selected = props.selected;
        linktitle = props.linktitle;
        const default_msg = (
                        <button type="button" className="navlist_unselected" data-type="navbutton" onClick={(e) => this.props.changeproject(linktitle, e)}><div className="NavButton">{linktitle}</div></button>
            );
        if(is_selected == null){
            if(this.state.debug) console.log(`NavList.linkbutton: Selected view not defined.`);
            return default_msg;
        }
        if(this.state.debug) console.log(`NavList.linkbutton: Is currently shown item? Comparing ${is_selected.Link_Title} with ${linktitle}`);
        if(linktitle == is_selected.Link_Title){
            return (
                    <button type="button" className="navlist_selected" data-type="navbutton" onClick={(e) => this.props.changeproject(linktitle, e)}><div className="NavButton">{linktitle}</div></button>
            )
        } else {
            return default_msg;
        } 
    }

    prepare() {
        if(this.props.projects == null){
            console.error("NavList.prepare: No projects found. Cannot prepare a list");
            return;
        }
        var items = this.props.projects;
        if(items == null){
            console.error("NavList.prepare: Cannot render list. No projects available.");
            return;
        }
        if(this.state.debug) console.log("NavList.prepare: Projects: "), console.info(items);
        
        if(this.state.debug) console.log("NavList.linkbutton: Rendering link buttons");
        items.sort(function(a,b){
            if(a.Title < b.Title) return -1;
            if(a.Title > b.Title) return 1;
            return 0;
        })
        return items.map((project) =>
            <li key={project.Link_Title}>
                <this.linkbutton selected={this.props.c_project} linktitle={project.Link_Title} />
            </li>
        );
    }

    render() {
        if(this.state.debug) console.log("NavList.render: Rendering viewer component");
        var projects = this.prepare();
        return (
            <div id="NavList_Box">
                <ul>
                    {projects}
                </ul>
            </div>
        );
    }
}

//ContentViewer: Component shows specific information about the shown project
export class ContentViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
            emptyViewError: null,
            lostViewError: null,
        };
    }
    componentWillMount(){
        this.setErrors()
        this.state.debug = this.props.debug;
        if(this.state.debug) console.log("ContentViewer.componentWillMount: Mounting component"), console.log(this.props);        
    }

    //setErros: Before component is rendered, set error messages
    setErrors() {
        const emptyViewError = 
        (<div id="ContentViewer">
            <h2 className="AlertHeader">{this.props.lang.SelectAProjectFromNav}</h2>
        </div>);

        const lostViewError = 
        (<div id="ContentViewer">
            <h2 className="AlertHeader">{this.props.lang.ProjectNotAvailableLanguage}</h2>
        </div>);

        this.state.emptyViewError = emptyViewError;
        this.state.lostViewError = lostViewError;
    }

    prepare() {
        this.setErrors()
        if(this.props.c_project == null) {
            console.info("ContentViewer.prepare: No Project defined. Printing empty view");
            return this.state.emptyViewError;
        }
        else if(!this.props.found) {
            console.info("ContentViewer.prepare: Previous project lost");
            return this.state.lostViewError;    
        }
        var project = this.props.c_project;
        if(this.state.debug) console.log("ContentViewer.prepare: Setting up content view. Project data: "), console.info(project);
        var images;
        
        console.log(project)
        ////Create element from react class and render it to "header" div.
        var Component = React.createFactory(require("./gallery.js"));
        var settings = {debug: this.props.debug, pictures: project.Images};
        return (<div id="ContentViewer">
            <h1>{project.Title}</h1>
            {this.props.lang.ProjectAvailableAt}: <br/> 
            <a href={project.Link}>{project.Link}</a><br />
            {this.props.lang.Course}: {project.Course}, {project.YearCreated}<br />
            {this.props.lang.ProjectType}: {project.ProjectType}<br/>
            {this.props.lang.GroupSize}: {project.DevGroupSize}<br/>
            <h2>{this.props.lang.ShortDescription}</h2>
            <p>{project.Description}</p>
            <h2>{this.props.lang.Screenshots}</h2>
            {Component(settings)}
        </div>);
    }

    render() {
        var view = this.prepare();
        if(this.state.debug) console.log("ContentViewer.render: Rendering viewer component"), console.log(this.props);
        return (
            <div>
                {view}
                <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
            </div>
            
        );
    }
    componentDidMount() {
        
        
    }
}

