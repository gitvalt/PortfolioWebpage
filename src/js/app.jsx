import * as React from 'react';
import ReactDOM from 'react-dom';
import {Mainpage} from "./mainpage.jsx"

//hide loading box
document.getElementById("loading_Box").style.visibility = "hidden";
//Render app
ReactDOM.render(
    <Mainpage title="Valtteri Seuranen" />,
    document.getElementById('header')
);

console.info("App should now be rendered in div with id 'header'");
