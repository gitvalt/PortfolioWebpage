import * as React from 'react';


import "../css/common.scss";
import "../css/tabulator.scss";

import template, { SkillList } from "../config/templates.jsx";

//Tabulator: Component contains different tabs with different types of views.
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining if logs will be printed
    settings = settings from mainpage
*/
export class Tabulator extends React.Component {
    constructor(props) {
        super(props);
        this.changeComponent.bind(this);
        this.state = {
            debug: false,
            settings: null,
        };
    }
    componentWillMount() {
        this.state.debug = this.props.debug;
        if (this.state.debug) console.log("Tabulator.render: Rendering Tabulator"), console.info(this.state), console.log(this.props);
        if (this.props.settings != null) this.state.settings = this.props.settings;
        if (this.state.active == null) this.state.active = this.getView(this.state.settings.ContentBoxDefault);
    }

    //getView: Returns defined component or Error if view not found
    getView(component) {
        var errors = this.props.lang.Errors;
        if (this.state.debug) console.log("Tabulator.changeComponent: Click! Got component: " + component);
        if (component == null) {
            console.error("Tabulator.getComponents: Components undefined");
            return <Error title={errors.LoadingFailed.Title} msg={errors.LoadingFailed.Content} />;
        }
        var result, views;
        views = this.state.settings.ContentViews;
        switch (component) {
            case views.Description:
                result = <AboutMe debug={this.state.debug} lang={this.props.lang} />;
                break;
            case views.Skills:
                result = <SkillsComponent debug={this.state.debug} lang={this.props.lang} />;
                break;
            case views.Experience:
                result = <WorkExperiences debug={this.state.debug} lang={this.props.lang} />;
                break;
            case views.Education:
                result = <EducationsComponent debug={this.state.debug} lang={this.props.lang} />;
                break;
            default:
                console.error("Tabulator.getComponents: Component is unknown. Do not know which component to load!");
                result = <Error title={errors.LoadingFailed.Title} msg={errors.LoadingFailed.Content} />;
                break;
        }
        return result;
    }

    //changeComponent: set new active view --> rerender component
    changeComponent(component, event) {
        //console.clear();
        var view = this.getView(component);
        this.setState({ active: view });
    }

    render() {
        //<li><button onClick={(e) => this.changeComponent(this.state.settings.ContentViews.Experience, e)}>{this.props.lang.WorkExperiencesL}</button></li>
        return (
            <div id="tabulator_Box">
                <div id="tabulator_options_Box">
                    <ul>
                        <li><button id="description_button" onClick={(e) => this.changeComponent(this.state.settings.ContentViews.Description, e)}>{this.props.lang.AboutMe}</button></li>
                        <li><button id="skills_button" onClick={(e) => this.changeComponent(this.state.settings.ContentViews.Skills, e)}>{this.props.lang.SkillsAndTalents}</button></li>
                        <li><button id="school_button" onClick={(e) => this.changeComponent(this.state.settings.ContentViews.Education, e)}>{this.props.lang.Education}</button></li>
                    </ul>
                </div>
                <div id="tabulator_focus"></div>
                <div id="shownContent">
                    {this.state.active}
                </div>
            </div>
        );
    }
}

//AboutMe: Component showing small description about the user
/*
    Properties:
    lang = object with language phrases
    debug = boolean defining if logs will be printed
*/
export class AboutMe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }
    render() {
        this.state.debug = this.props.debug;
        if (this.state.debug) console.log("AboutMe.render: Rendering AboutMe component");
        return (
            <div id="Description_box">
                <p>{this.props.lang.PersonDescription}</p>
            </div>
        );
    }
}


//SkillsComponent: Component list of skills where user has experience with.
/*
    Skills are read from language object 'lang.Skills'

    Properties:
    lang = object with language phrases
    debug = boolean defining if logs will be printed
*/
export class SkillsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }

    //parseSkills: returns skills as list of jsx components 
    parseSkills() {
        if (this.state.debug) console.log("Skills.parseSkills: Parsing skills to jsx format...");
        var lang = this.props.lang;
        var error_content = lang.Errors.SkillLoadFailed.Title + " " + lang.Errors.SkillLoadFailed.Content;
        var errmsg = <Error title={lang.Errors.SkillLoadFailed.Title} msg={lang.Errors.SkillLoadFailed.Content} />
        var skills = this.props.lang.Skills;
        if (this.state.debug) console.info(skills);
        if (skills == null || skills.lenght == 0) {
            console.error("Skills.parseSkills: props.Skills is empty");
            return errmsg;
        }
        if (!(skills instanceof Array)) {
            console.error("Skills.parseSkills: props.Skills is not an Array");
            return errmsg;
        }

        var skill = new SkillList(lang.Skills);
        var items = [];
        var tags = skill.GetTags();
        if (this.state.debug) console.log("Skills.parseSkills: Received tags:"), console.info(tags);
        
        var handledTags = [];
        tags.forEach(tag => {
            
            if(!handledTags.includes(tag)){
                var elements = skill.GetItemsWithTag(tag);
                items.push(elements);
                handledTags.push(tag)
            }

        });
        if (this.state.debug) console.log(`Skills.parseSkills: Handled total of '${items.length}' categories`);
        return items.map((skill) =>
            <div key={skill.Title}>
                <h1>{skill.Title}</h1>
                <ul>
                    {
                        skill.Values.map((item) =>
                            <li key={item.Title}>{item.Title}</li>
                        )
                    }
                </ul>
            </div>
        );
    }
    render() {
        this.state.debug = this.props.debug;
        if (this.state.debug) console.log("Skills.render: Rendering skills");
        var skills = this.parseSkills();
        return (
            <div id="skills_Box">
                <ul>
                    {skills}
                </ul>
            </div>
        );
    }

}


//WorkExperiences: Component similar to SkillsComponent, showns list of work experiences.
export class WorkExperiences extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }
    parseExp() {
        if (this.state.debug) console.log("WorkExperience.parseExp: Parsing experiences to jsx format...");
        var lang = this.props.lang;
        var items = this.props.lang.WorkExperiences;
        if (this.state.debug) console.info(items);
        if (items == null || items.lenght == 0) {
            console.error("WorkExperiences.parseExp: experiences are empty");
            return null;
        }

        if (this.state.debug) console.log(`WorkExperience.parseExp: Handled total of '${items.length}' work exp's`);
        return items.map((exp) =>
            <li key={exp.Title}>
                <div key={exp.Title}>
                    <h3>{exp.Title}</h3>
                    {lang.OnTimespan}: {exp.Time}<br />
                    <p>{exp.Description}</p>
                </div>
            </li>
        );
    }
    render() {
        this.state.debug = this.props.debug;
        if (this.state.debug) console.log("WorkExperience.render: Rendering skills");
        var work = this.parseExp();
        return (
            <div id="WorkExperience_Box">
                <h2>{this.props.lang.WorkExperiencesL}:</h2>
                <ul>
                    {work}
                </ul>
            </div>
        );
    }

}


//EducationComponent: Component similar to SkillsComponent, showns list of previous educations.
export class EducationsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            debug: false,
        };
    }
    parseEdc() {
        if (this.state.debug) console.log("EducationsComponent.parseEdc: Parsing educations to jsx format...");
        var lang = this.props.lang;
        var items = this.props.lang.Educations;
        if (this.state.debug) console.info(items);
        if (items == null || items.lenght == 0) {
            console.error("EducationsComponent.parseEdc: educations are empty");
            return null;
        }
        if (this.state.debug) console.log(`EducationsComponent.parseEdc: Handled total of '${items.length}' educations`);
        return items.map((edu) =>
            <div>
                <h3>{edu.Degree}</h3>
                {lang.OnTimespan}: {edu.Timespan}<br />
                {lang.Location}: {edu.School}, {edu.Location}<br />
                <p>{edu.Description}</p>
            </div>
        );
    }
    render() {
        this.state.debug = this.props.debug;
        if (this.state.debug) console.log("EducationComponent.render: Rendering skills");
        var work = this.parseEdc();
        return (
            <div id="Education_Box">
                <h2>{this.props.lang.Education}:</h2>
                <ul>
                    {work}
                </ul>
            </div>
        );
    }

}