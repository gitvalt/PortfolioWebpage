const path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/js/app.jsx',
  cache: true,
  mode: "production",
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules:
    [
        {
          test: [/\.js$/, /\.es6$/, /\.jsx$/],
          exclude: /node_modules/,
          use: {
              loader: 'babel-loader',
              options: {
                  presets: ['es2015', 'babel-preset-react']
                }
          }
        },
        
        {
          test: /\.(jpg|png|svg)$/,
          use: {
            loader: "url-loader",
            options: {
              limit: 25000,
            },
          },
        },
        {
          test:/\.(s*)css$/,
          use:['style-loader', 'css-loader', 'sass-loader']
        }
    ]
  }
};
